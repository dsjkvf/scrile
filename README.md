# scrile

## About

video **SCR**eenshots t**ILE** generator is a simple bash wrapper around `exiftool` and `ffmpeg`, which takes 20 screenshots per provided video file and then generates a thumbnail sheet.

## Usage

    scrile video.mp4
